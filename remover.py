import sys, os

input_file_path = sys.argv[1]

with open(input_file_path) as f:
  lines = f.readlines()

print('len = ',len(lines))

f = open(input_file_path, 'w')

for line in lines:
  video_name = line.split('/')[-1]
  
  splitted = video_name.split('_')
  hour = splitted[4]
  min = splitted[5]
  if hour == '08':
    if min != '00' and min != '05':
      f.write(line)
  elif hour == '09':
    if min == '00' or min == '05' or min == '10' or min == '15':
      f.write(line)
  else:
    pass