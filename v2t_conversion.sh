#!/bin/sh
# added by Anaconda3 installer
export LD_LIBRARY_PATH=/usr/local/cuda-9.1/lib64:/usr/lib64:/usr/local/lib\${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
# added by Anaconda3 installer
export PATH="/data/anaconda3/bin:$PATH"
SOURCE="cnbctv18"
DATE=`date +%Y_%m_%d`
HOME_DIR=/data/binil/v2t_final
PROCESSED_DIR=$HOME_DIR/processed_data


SOURCE_DIR_PATH="$PROCESSED_DIR/${SOURCE}_${DATE}*"
FINAL_DIR=$(TZ='Asia/Kolkata' date +%Y_%m_%d_%H_%M_%S)
FINAL_PATH=$HOME_DIR/output/$SOURCE/$FINAL_DIR
TMP_DIR=$FINAL_PATH/tmp
TMP_IMG_DIR=$TMP_DIR/image
MEREGED_VIDEO=$TMP_DIR/merged.avi
CROPPED_LIB_PATH=$HOME_DIR/libs/cropped
VISION_LIB_PATH=/data/yaser/yaser_testing/cropped
SOURCE=cnbctv18
CURRENT_DATE=`date +%Y-%m-%d`


mkdir $FINAL_PATH
mkdir $TMP_DIR
mkdir $TMP_IMG_DIR
mv $SOURCE_DIR_PATH $TMP_DIR/
python $HOME_DIR/make_list.py $TMP_DIR
python $HOME_DIR/remover.py $TMP_DIR/mylist.txt
ffmpeg -safe 0 -y -f concat -i $TMP_DIR/mylist.txt -vcodec h264 -vf fps=25 -b:v 1M -b:a 639k $MEREGED_VIDEO
python $VISION_LIB_PATH/vision.py $MEREGED_VIDEO $TMP_IMG_DIR
python $VISION_LIB_PATH/check-cropped.py $TMP_DIR $TMP_IMG_DIR $CROPPED_LIB_PATH
python $VISION_LIB_PATH/process.py $TMP_DIR $MEREGED_VIDEO $FINAL_PATH $SOURCE $CURRENT_DATE
