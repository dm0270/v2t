import sys,os

source = sys.argv[1]
dest = sys.argv[2]

if not os.path.exists(dest):
  os.mkdir(dest)

dirs = os.listdir(source)

for dir in dirs:
  time = dir.split('_')[2]
  splitted = time.split('-')
  hour = splitted[0]
  min = splitted[1]
  if hour == '08' and (min != '00' or min != '05'):
    print(dir)
    os.rename(source+dir,dest+dir)
  if hour == '09' and (min == '00' or min == '05' or min == '10' or min == '15'):
    print(dir)
    os.rename(source+dir,dest+dir)


