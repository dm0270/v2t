import sys,os

source = sys.argv[1]
dest = sys.argv[2]

if not os.path.exists(dest):
  os.mkdir(dest)

dirs = os.listdir(source)
for dir in dirs:
  splitted = dir.split('_')
  hour = splitted[4]
  min = splitted[5]
  if hour == '08':
    if min == '00' or min == '05':
      os.rename(source+'/'+dir,dest+'/'+dir)
  elif hour == '09':
    if min != '00' and min != '05' and min != '10' and min != '15':
      os.rename(source+'/'+dir,dest+'/'+dir)
  else:
    os.rename(source+'/'+dir,dest+'/'+dir)

