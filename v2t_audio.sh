#!/bin/sh
# added by Anaconda3 installer
export LD_LIBRARY_PATH=/usr/local/cuda-9.1/lib64:/usr/lib64:/usr/local/lib\${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}


# added by Anaconda3 installer
export PATH="/data/anaconda3/bin:$PATH"
HOME_DIR=/data/binil/v2t_final
PROCESSED_DIR=$HOME_DIR/processed_data
RAW_DIR=$HOME_DIR/raw_data
#rm -rf /home/pawan/ASR_NLP/Audio_Chunks/*.*
SOURCE="$1"
DATE="$2"
STREAMED_VIDEO="$3"
mv -f $STREAMED_VIDEO $RAW_DIR
VIDEO1=${STREAMED_VIDEO##*/}  # retain the part after the last slash
OUT_DIR=$PROCESSED_DIR/${VIDEO1%.*}/
TEMP_OUT_DIR=$OUT_DIR/temp
mkdir  $OUT_DIR
WAV_FILE="$OUT_DIR/${VIDEO1%.*}.wav"
echo $WAV_FILE
VIDEO=$RAW_DIR/$VIDEO1
ffmpeg -i $VIDEO -acodec pcm_s16le -ar 16000 -ac 1 $WAV_FILE
mv -f $VIDEO $OUT_DIR
#ffmpeg -i $WAV_FILE -af "highpass=f=200, lowpass=f=4000" $OUT_DIR/1.wav
#ffmpeg -i 1.wav -af "volume=5dB" $OUT_DIR/2.wav



python2 /data/pawan/pawan/audfprint-master/ad_cnbc_2.py $OUT_DIR



FILE_LIST=`find "$TEMP_OUT_DIR" -maxdepth 1  -name "*.avi"`
for i in $FILE_LIST
do
        #rm -rf /home/pawan/ASR_NLP/Audio_Chunks/*.*
        PART_VIDEO=${i##*/}
        PART_VIDEO_DIR=${PART_VIDEO%.*}
        PART_DIR=$OUT_DIR/$PART_VIDEO_DIR
        mkdir $PART_DIR
        echo $PART_DIR
        mv $i $PART_DIR
        mv $TEMP_OUT_DIR/$PART_VIDEO_DIR.wav $PART_DIR
        PART_WAV_FILE=$PART_DIR/$PART_VIDEO_DIR.wav
        echo $PART_WAV_FILE
#        python3.6 /home/pawan/ASR_NLP/NLP/Video_Audio_Chunks_1.py $PART_WAV_FILE
        #python /data/binil/English_ver1/deepspeech.pytorch/transcribe.py  --decoder beam --cuda --source $SOURCE --date $DATE --video $PART_WAV_FILE --out_dir $PART_DIR
        
done

rm -rf $TEMP_OUT_DIR
## for loop:
   #python3.6 /home/pawan/ASR_NLP/NLP/Video_Audio_Chunks_1.py $WAV_FILE
   #python /data/binil/English_ver1/deepspeech.pytorch/transcribe.py  --decoder beam --cuda --source $SOURCE --date $DATE --video $WAV_FILE

