import glob
import sys

tmp_dir = sys.argv[1]
avi_dir = tmp_dir + '/*/*/*.avi'
f = open(tmp_dir+'/mylist.txt','w')
files = glob.glob(avi_dir)
files.sort()
for file in files:
    f.write('file '+file+'\n')
f.close()
