#!/bin/sh
STREAM_DIR=/data/binil/v2t_final/stream_data
RAW_DIR=/data/binil/v2t_final/raw_data
PROCESSED_DIR=/data/binil/v2t_final/processed_data
SOURCE_NAME=cnbctv18
V2T_AUDIO=/data/binil/v2t_final/v2t_audio.sh
FILE_NAME=cnbctv18_$(TZ='Asia/Kolkata' date +%Y_%m_%d_%H_%M_%S).avi
CURRENT_HOUR=$(TZ='Asia/Kolkata' date +%H)
ps -ef|grep cnbc_tv18|awk {'print $2'} | xargs kill -9

if [ $CURRENT_HOUR -gt 7 ] && [ $CURRENT_HOUR -lt 10 ]
then 
    nohup ffmpeg -y -re -i "http://cnbc_tv18_hls-lh.akamaihd.net/i/cnbc_tv18_hls_n_1@174868/index_3_av-p.m3u8?sd=10&rebase=on&hdntl=exp=1522322763~acl=%2f*~data=hdntl~hmac=24842f97b37e3304671748642e9e0ca8333b776b9a1a24e5cbd3cc4d77b3efa9"  -acodec copy -vcodec copy  -r 25 $STREAM_DIR/$FILE_NAME &
fi
FILE_LIST=`find "$STREAM_DIR" -maxdepth 1  -name "*.avi"`
OPEN_FILES=`lsof +D "$STREAM_DIR"` > openFiles.txt

for i in $FILE_LIST
do
        if [ `grep $i openFiles.txt` ];
        then
             echo $i 
        else
             CURRENT_DATE=`date +%Y-%m-%d`
             sh $V2T_AUDIO $SOURCE_NAME $CURRENT_DATE $i
        fi
done
